# -*- coding: utf-8 -*-

from odoo import models, fields, api
import ldap

class Auth_ldap_implicit(models.Model):
    _inherit = 'res.company.ldap'

    ldap_use_starttls = fields.Boolean(default=True,
                                       help="unset if your specified port is a dedicated ldaps port",
                                       string="Use STARTTLS")
    is_show_ldap_use_starttls = fields.Boolean(string="is show ldap_use_starttls")

    @api.multi
    def get_ldap_dicts(self):
        """
        Retrieve res_company_ldap resources from the database in dictionary
        format.
        :return: ldap configurations
        :rtype: list of dictionaries
        """

        ldaps = self.sudo().search([('ldap_server', '!=', False)], order='sequence')
        res = ldaps.read([
            'id',
            'company',
            'ldap_server',
            'ldap_server_port',
            'ldap_binddn',
            'ldap_password',
            'ldap_filter',
            'ldap_base',
            'user',
            'create_user',
            'ldap_tls',
            'ldap_use_starttls'
        ])
        return res

    def connect(self, conf):
        """
        Connect to an LDAP server specified by an ldap
        configuration dictionary.

        :param dict conf: LDAP configuration
        :return: an LDAP object
        """

        if conf['ldap_tls'] and not conf['ldap_use_starttls']:
            uri = 'ldaps://%s:%d' % (conf['ldap_server'], conf['ldap_server_port'])
            connection = ldap.initialize(uri)
        elif conf['ldap_tls'] and conf['ldap_use_starttls']:
            uri = 'ldap://%s:%d' % (conf['ldap_server'], conf['ldap_server_port'])
            connection = ldap.initialize(uri)
            connection.start_tls_s()
        else:
            uri = 'ldap://%s:%d' % (conf['ldap_server'], conf['ldap_server_port'])
            connection = ldap.initialize(uri)

        return connection


    @api.onchange('ldap_tls')
    def ldap_tls_checked(self):
        if self.ldap_tls:
            self.is_show_ldap_use_starttls = True
        else:
            self.is_show_ldap_use_starttls = False
