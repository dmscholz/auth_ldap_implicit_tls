# -*- coding: utf-8 -*-
{
    'name': "auth_ldap_implicit_tls",

    'summary': """
        This module adds the option of using implicit tls for ldap authentication""",

    'description': """
        Your ldap server uses a dedicated ldaps port only? Starttls is not supported?
        This module gives you the option of using implicit tls for ldap authentication. You can decide whether 
        starttls should be used or not.        
    """,

    'author': "dmscholz",
    'website': "https://gitlab.com/dmscholz/auth_ldap_implicit",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Extra Tools',
    'version': '0.1',
    'depends': ['auth_ldap'],
    'data': [
        'views/config_settings_views.xml'
    ],
    'images': ['images/no_starttls_screenshot.png', 'images/no_tls_screenshot.png'],
    'external_dependencies': {
        'python': ['ldap'],
    }
}